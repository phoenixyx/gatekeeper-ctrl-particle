#include "application.h"
#include "neopixel.h"

#include "Adafruit_PN532.h"

SYSTEM_THREAD(ENABLED);

const int OPEN_PIN = D2;
const int RELAY_PIN = D4;
const int LED_PIN = D5;
const int BUZZER_PIN = D6;

#define SCK  (A3)
#define MOSI (A5)
#define SS   (A2)
#define MISO (A4)

const int RELAY_ACTIVATE_STATE = HIGH;

// Var to store the last read uid in so we dont spam MQTT
int lastCardUid[] = { 0, 0, 0, 0, 0, 0, 0 };
unsigned long lastRead = 0;
unsigned long lastReadSuccess = 0;

// Track durations
unsigned long openAt = 0;
unsigned long openDuration = 0;

int readerLedSetAt = 0;
int readerLedDuration = 500;

int buzzerAt = 0;
int buzzerDuration = 500;

const uint8_t S_LED = 0;
const uint8_t R_LED = 1;

Thread* nfcThread;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(2, LED_PIN, WS2812B);
Adafruit_PN532 nfc(SCK, MISO, MOSI, SS);

void pointActivate(int duration = 5000) {
  openAt = millis();
  openDuration = duration;
  digitalWrite(RELAY_PIN, HIGH);

  String msg = "Opened for " + duration;
  Serial.println(msg);
}

void pointTimeout() {
  if ((millis() - openAt) >= openDuration) {
    digitalWrite(RELAY_PIN, LOW);
  }
}

void setReaderLed(int duration = 600, uint8_t r = 255, uint8_t g = 255, uint8_t b = 255) {
  pixels.setPixelColor(R_LED, pixels.Color(r, g, b));
  Serial.print(millis());
  Serial.println("Setting Reader LED");
  pixels.show();
  readerLedDuration = duration;
  readerLedSetAt = millis();
}

void readerLedTimeout() {
  if ((millis() - readerLedSetAt) >= readerLedDuration) {
    pixels.setPixelColor(R_LED, pixels.Color(255,255,255));
  }
  pixels.show();
}

void buzzerSet(int duration = 600) {
  digitalWrite(BUZZER_PIN, HIGH);
  buzzerAt = millis();
  buzzerDuration = duration;
}

void buzzerTimeout() {
  if ((millis() - buzzerAt) > buzzerDuration) {
    digitalWrite(BUZZER_PIN, LOW);
  }
}

void getUidChars(uint8_t uid[], uint8_t uidLength, char* result, const unsigned result_length) {
  char part[] = {};
  for (int i = 0; i < uidLength; i++) {
    sprintf(part, "%02x", uid[i]);
    strcat(result, part);
  }
}

void readRFID() {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success) {
    /*
    Is it the same card that was read then we skip over the rest so we dont
     to verify it below
    */
    int isSame = memcmp(lastCardUid, uid, uidLength);

    if (isSame == 0) {
      Serial.println("Same card detected, skipping");
      return;
    };

    memcpy(lastCardUid, uid, uidLength);

    lastReadSuccess = millis();

    // Display some basic information about the card
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");
    Serial.print(uidLength, DEC);
    Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);

    if (uidLength == 4) {
      // We probably have a Mifare Classic card ...

      char id[100] = {};
      int idLen = 0;
      getUidChars(uid, uidLength, id, idLen);

      String credId = String(id);
      String json = "{\"event\": \"verify\", \"credential_id\": \"" + credId + "\"}";
      Particle.publish("message", json, 60, PRIVATE);
    }

    Serial.println("");
    return;
  }

  // In case of there wasn't a successfull attempt then we null the lastCardUid
  if ((millis() - lastReadSuccess) > 2000) {
    lastCardUid[0] = 0;
  }
}

os_thread_return_t nfcLoop() {
  for (;;) {
    readRFID();
  }
}

int commandHandler(String str) {
    if (str == "OPEN") {
      pointActivate();
      setReaderLed(4000, 0,255,0);
      buzzerSet(1000);
    } else if (str == "DENIED") {
      setReaderLed(2000, 255,0,0);
    }
}

void setup() {
  Serial.begin(9600);

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  pixels.begin();
  pixels.setBrightness(100);
  pixels.setPixelColor(0, pixels.Color(255,255,255));
  pixels.setPixelColor(1, pixels.Color(255,255,255));
  pixels.show();

  delay(400);

  nfc.begin();

  uint32_t versiondata;
  do {
      versiondata = nfc.getFirmwareVersion();
      if (!versiondata) {
          Serial.print("Didn't find PN53x board");
          delay(1000);
          Particle.process(); // Keep connected to the Cloud while re-trying
      }
  }
  while(!versiondata);

  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  nfc.setPassiveActivationRetries(0xFF);

  Particle.function("command", commandHandler);

  nfcThread = new Thread("nfc", nfcLoop);
}

// Do our main stuff here
void loop() {
  pointTimeout();
  buzzerTimeout();
  readerLedTimeout();
}
